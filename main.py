import sys
import random

from utils import messages
from utils.interpreter import Interpreter
from chat.server import Server
from chat.client import ClientManager

DEFAULT_PORT_RANGE = xrange(5000, 6000)
DEFAULT_HOST = ''


def generate_random_port():
    return random.choice(DEFAULT_PORT_RANGE)


def main():
    if len(sys.argv) == 1:
        port = generate_random_port()
    else:
        try:
            port = int(sys.argv[1])
        except ValueError:
            print messages.NOT_INT_PORT
            sys.exit(1)
    host = sys.argv[2] if len(sys.argv) == 3 else DEFAULT_HOST
    server = Server(port, host)
    server.start()
    client_manager = ClientManager()
    Interpreter(server, client_manager).start()


if __name__ == '__main__':
    main()
