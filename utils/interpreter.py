import sys

from utils import messages

from chat.base import Response, Request
from crypto_algo import *
from crypto_algo.standard.hash import main as standard_hash
from crypto_algo.pycrypto.hash import main as pycrypto_hash


class InterpreterException(Exception):
    pass


class Interpreter(object):
    COMMANDS_LIST = ['help', 'exit', 'show', 'add', 'send', 'set', 'test_hash']

    ALL_STRATEGY = [AESStrategy, ARC2Strategy, ARC4Strategy, BlowfishStrategy,
                    CastStrategy, DESStrategy, DES3Strategy, PKCS1_v1_5Strategy,
                    PKCS1_OAEPStrategy, FernetStrategy, XORStrategy]
    AVAILABLE_STRATEGY = {obj.__name__.split('.')[-1]: obj for obj in ALL_STRATEGY}

    def __init__(self, server, client_manager):
        self.server = server
        self.client_manager = client_manager

    @staticmethod
    def help():
        print messages.HELP_TEXT.format(', '.join(Interpreter.AVAILABLE_STRATEGY.keys()))

    def exit(self):
        self.server.stop()
        sys.exit()

    def show(self):
        self.client_manager.show()

    def add(self, name, port, host=''):
        try:
            port = int(port)
            self.client_manager.add(name, port, host)
        except ValueError:
            print messages.NOT_INT_PORT

    def send(self, name, data):
        recipient = self.client_manager.get(name)
        if recipient:
            recipient.send(dict(message=data))
        else:
            print messages.NO_SUCH_RECIPIENT

    def set(self, strategy):
        if strategy in self.AVAILABLE_STRATEGY:
            Response.current_strategy = self.AVAILABLE_STRATEGY[strategy]
            Request.current_strategy = self.AVAILABLE_STRATEGY[strategy]
        else:
            print messages.ERROR_STRATEGY

    @staticmethod
    def test_hash(message):
        standard_hash(message)
        pycrypto_hash(message)

    @staticmethod
    def parse_command(text):
        command_parts = text.split()
        if not command_parts:
            raise InterpreterException
        return command_parts[0], command_parts[1:]

    def start(self):
        while True:
            try:
                text = raw_input(messages.INPUT_MESSAGE)
                try:
                    command, args = self.parse_command(text)
                    if command not in self.COMMANDS_LIST:
                        raise InterpreterException
                    getattr(self, command)(*args)
                except InterpreterException:
                    print messages.INTERPRETER_EXCEPTION_MESSAGE
                except TypeError:
                    print messages.INVALID_ARGUMENTS
            except KeyboardInterrupt:
                self.exit()
