import re

tag = '<{0}>{1}</{0}>'
tag_regex = re.compile(r'<([^/]+?)>')
text_regex = r'<{0}>(.*)</{0}>'


def XMLbuilder(object):
    return tag.format('body', _build(object))


def XMLparser(xml):
    result = dict()
    xml = xml[6:-7]
    for group in tag_regex.findall(xml):
        text = re.findall(text_regex.format(group), xml, flags=re.DOTALL)[0]
        if text.isdigit():
            text = int(text)
        result[group] = text
    return result


def _build(object):
    if not isinstance(object, dict):
        raise ValueError
    text = ''
    for key in object:
        text += tag.format(key, object[key])
    return text
