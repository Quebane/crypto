import os
import sys

from Crypto.PublicKey import RSA
import messages

home = os.path.expanduser("~")

DEFAULT_RSA_WAY = os.path.join(home, '.ssh', 'id_rsa')
DEFAULT_RSA_PUB_WAY = DEFAULT_RSA_WAY + '.pub'


def get_rsa_key():
    try:
        rsa = open(DEFAULT_RSA_WAY).read()
        rsa_pub = open(DEFAULT_RSA_PUB_WAY).read()
        return rsa, rsa_pub
    except IOError:
        try:
            key = RSA.generate(2048)
            f = open(DEFAULT_RSA_WAY, 'w')
            f.write(key.exportKey('PEM'))
            f.close()
            f = open(DEFAULT_RSA_PUB_WAY, 'w')
            f.write(key.publickey().exportKey('PEM'))
            f.close()
            return get_rsa_key()
        except Exception:
            print messages.RSA_ERROR
            sys.exit(1)
