NOT_INT_PORT = 'Sorry, port value must be an integer. Please try again.'
BIND_PORT_EXCEPTION = 'This port is occupied. Please try again.'
MESSAGE = '{1}:{2} - {0}'
INPUT_MESSAGE = 'Please enter a command or enter "help" to show all commands\n'
INTERPRETER_EXCEPTION_MESSAGE = 'Invalid command. Please try again.'
HELP_TEXT = """help - show help
exit - exit from application
add <name> <port> <host> - add new recipient, default host value = 'localhost'
send <name> <data> - send text to a recipient
send <data> - send text to all recipients
show - show all recipients
set <strategy> - set crypto strategy. Available strategy - {0}
test_hash <message> - test hash functions
"""
NO_SUCH_RECIPIENT = 'No such recipient. Please try again.'
INVALID_ARGUMENTS = 'Invalid function arguments. Please try again.'
CONNECT_EXCEPTION = 'Failed to connect with the recipient. Please try again.'
FAILED_SEND = 'Failed to send data. Please try again.'
RSA_ERROR = 'Can not find the RSA key. Please try again.'
ERROR_STRATEGY = 'No such strategy. Please try again.'
