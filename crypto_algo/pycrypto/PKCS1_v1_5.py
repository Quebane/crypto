from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA
from Crypto import Random

from crypto_algo.base import ASymmetricalStrategy


class PKCS1_v1_5Strategy(ASymmetricalStrategy):
    public_key = RSA
    algorithm = PKCS1_v1_5

    def encrypt(self):
        hash = SHA.new(self.message)
        self.data['message'] = self.crypto_obj.encrypt(self.message + hash.digest())
        self.data['real_size'] = len(self.message)
        print self.data

    def decrypt(self):
        dsize = SHA.digest_size
        sentinel = Random.new().read(int(self.data['real_size'])+dsize)
        self.data['message'] = self.crypto_obj.decrypt(self.message, sentinel)[:-dsize]
