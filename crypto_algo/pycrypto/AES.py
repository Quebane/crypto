from Crypto.Cipher import AES

from crypto_algo.base import BlockSymmetricalStrategy


class AESStrategy(BlockSymmetricalStrategy):
    key = 'D3iUZhsv9lZ8v9ny'
    BLOCK_SIZE = 16
    algorithm = AES

