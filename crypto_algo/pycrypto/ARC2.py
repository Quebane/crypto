from Crypto.Cipher import ARC2

from crypto_algo.base import BlockSymmetricalStrategy


class ARC2Strategy(BlockSymmetricalStrategy):
    key = 'D3iUZhsv'
    BLOCK_SIZE = 8
    algorithm = ARC2
