from Crypto.Cipher import DES

from crypto_algo.base import BlockSymmetricalStrategy


class DESStrategy(BlockSymmetricalStrategy):
    key = 'D3iUZhsv'
    BLOCK_SIZE = 8
    algorithm = DES
