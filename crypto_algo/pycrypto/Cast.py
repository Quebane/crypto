from Crypto.Cipher import CAST

from crypto_algo.base import BlockSymmetricalStrategy


class CastStrategy(BlockSymmetricalStrategy):
    key = 'D3iUZhsv'
    BLOCK_SIZE = 8
    algorithm = CAST
