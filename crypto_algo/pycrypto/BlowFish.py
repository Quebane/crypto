from Crypto.Cipher import Blowfish

from crypto_algo.base import BlockSymmetricalStrategy


class BlowfishStrategy(BlockSymmetricalStrategy):
    key = 'D3iUZhsv'
    BLOCK_SIZE = 8
    algorithm = Blowfish
