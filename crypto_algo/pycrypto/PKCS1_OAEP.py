from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA

from crypto_algo.base import ASymmetricalStrategy


class PKCS1_OAEPStrategy(ASymmetricalStrategy):
    public_key = RSA
    algorithm = PKCS1_OAEP
