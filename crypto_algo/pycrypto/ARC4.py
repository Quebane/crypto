from Crypto.Cipher import ARC4

from crypto_algo.base import StreamSymmetricalStrategy


class ARC4Strategy(StreamSymmetricalStrategy):
    key = 'Very long and confidential key'
    algorithm = ARC4
