from Crypto.Cipher import XOR

from crypto_algo.base import StreamSymmetricalStrategy


class XORStrategy(StreamSymmetricalStrategy):
    key = 'Very long and confidential key'
    algorithm = XOR
