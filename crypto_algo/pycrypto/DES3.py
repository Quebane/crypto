from Crypto.Cipher import DES3

from crypto_algo.base import BlockSymmetricalStrategy


class DES3Strategy(BlockSymmetricalStrategy):
    key = 'D3iUZhsv9lZ8v9ny'
    BLOCK_SIZE = 8
    algorithm = DES3
