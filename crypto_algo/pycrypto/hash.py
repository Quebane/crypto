from Crypto.Hash import *

MESSAGE = 'Lorem ipsum dolore sit amend'

all_algorighm = [MD2, MD4, MD5, RIPEMD, SHA, SHA224, SHA256, SHA384, SHA512]


def hashing(algo, message):
    m = algo.new()
    m.update(message)
    return m.hexdigest()


def main(message):
    print 'Message: {0}'.format(message)
    for algo in all_algorighm:
        print 'Algorithm {0} - result: {1}'.format(algo.__name__.split('.')[-1], hashing(algo, message))

if __name__ == '__main__':
    main(MESSAGE)
