import hashlib

MESSAGE = 'Lorem ipsum dolore sit amend'


def hashing(algo, message):
    m = hashlib.new(algo)
    m.update(message)
    return m.hexdigest()


def main(message):
    print 'Message: {0}'.format(message)
    for algo in hashlib.algorithms:
        print 'Algorithm {0} - result: {1}'.format(algo, hashing(algo, message))


if __name__ == '__main__':
    main(MESSAGE)
