from abc import ABCMeta, abstractmethod
import random


class Strategy(object):
    def __init__(self, data):
        self.data = data
        self.message = self.data['message']

    @abstractmethod
    def encrypt(self):
        pass

    @abstractmethod
    def decrypt(self):
        pass

    __metaclass__ = ABCMeta


class SymmetricalStrategy(Strategy):
    KEY_SIZE = 16
    ALPHABET = [chr(char) for char in xrange(ord('A'), ord('z')) if char <= 90 or char >= 97]
    ALPHABET += [str(char) for char in xrange(0, 10)]

    @classmethod
    def generate_key(cls):
        cls.key = ''.join([random.choice(cls.ALPHABET) for i in xrange(0, cls.KEY_SIZE)])

    __metaclass__ = ABCMeta


class StreamSymmetricalStrategy(SymmetricalStrategy):
    algorithm = None

    def __init__(self, data):
        super(StreamSymmetricalStrategy, self).__init__(data)
        self.crypto_obj = self.algorithm.new(self.key)

    def encrypt(self):
        self.data['message'] = self.crypto_obj.encrypt(self.message)

    def decrypt(self):
        self.data['message'] = self.crypto_obj.decrypt(self.message)

    __metaclass__ = ABCMeta


class BlockSymmetricalStrategy(SymmetricalStrategy):
    algorithm = None

    def __init__(self, data):
        super(BlockSymmetricalStrategy, self).__init__(data)
        self.crypto_obj = self.algorithm.new(self.key, self.algorithm.MODE_ECB)

    def encrypt(self):
        message_len = len(self.message)
        if message_len % self.BLOCK_SIZE:
            self.message += 'a' * (self.BLOCK_SIZE - message_len % self.BLOCK_SIZE)
            self.data['real_len'] = message_len
        self.data['message'] = self.crypto_obj.encrypt(self.message)

    def decrypt(self):
        self.data['message'] = self.crypto_obj.decrypt(self.message)
        if 'real_len' in self.data:
            self.data['message'] = self.data['message'][:self.data['real_len']]

    __metaclass__ = ABCMeta


class ASymmetricalStrategy(Strategy):
    algorithm = None

    def __init__(self, data, rsa=None, rsa_pub=None):
        super(ASymmetricalStrategy, self).__init__(data)
        key = None
        if rsa:
            key = rsa
        if rsa_pub:
            key = rsa_pub
        if key:
            self.key = self.public_key.importKey(key)
            self.crypto_obj = self.algorithm.new(self.key)

    def encrypt(self):
        self.data['message'] = self.crypto_obj.encrypt(self.message)

    def decrypt(self):
        self.data['message'] = self.crypto_obj.decrypt(self.message)
