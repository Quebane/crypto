from cryptography.fernet import Fernet

from crypto_algo.base import SymmetricalStrategy


class FernetStrategy(SymmetricalStrategy):
    key = 'VZEHGjnyp_NLLxDrGsLhIuT_zmcRmYDp2vXCkmEIuBQ='

    def __init__(self, data):
        super(FernetStrategy, self).__init__(data)
        self.crypto_obj = Fernet(self.key)

    def encrypt(self):
        self.data['message'] = self.crypto_obj.encrypt(self.message)

    def decrypt(self):
        self.data['message'] = self.crypto_obj.decrypt(self.message)
