from abc import ABCMeta
from crypto_algo import *

from utils.XML import XMLbuilder, XMLparser


class BaseQuery(object):
    current_strategy = None

    @property
    def strategy(self):
        if self.current_strategy:
            return self.current_strategy
        else:
            return AESStrategy

    def __init__(self, data, rsa=None, rsa_pub=None):
        self.data = data
        self.rsa = rsa
        self.rsa_pub = rsa_pub

    def parse_xml(self):
        return XMLparser(self.data)

    def encode_xml(self):
        return XMLbuilder(self.data)

    def parse(self):
        data = self.parse_xml()
        if 'message' in data:
            args = [data]
            if getattr(self.strategy, 'public_key', None):
                args.append(self.rsa)
                args.append(self.rsa_pub)
            self.strategy(*args).decrypt()
        return data

    def encode(self):
        if 'message' in self.data:
            args = [self.data]
            if getattr(self.strategy, 'public_key', None):
                args.append(self.rsa)
                args.append(self.rsa_pub)
            self.strategy(*args).encrypt()
        return self.encode_xml()

    __metaclass__ = ABCMeta


class Request(BaseQuery):
    pass


class Response(BaseQuery):
    SUCCESS_STATUS = 0
    FAILED_STATUS = 1

    def __init__(self, data=None, status=SUCCESS_STATUS, rsa=None, rsa_pub=None):
        if data is None:
            data = dict()
        super(Response, self).__init__(data, rsa=rsa, rsa_pub=rsa_pub)
        self.status = status

    def encode(self):
        self.data['status'] = self.status
        return super(Response, self).encode()
