import socket

from utils import messages
from base import Request, Response


class Client(object):
    MAX_BUFFER_SIZE = 8096

    def __init__(self, port, host):
        self.port = port
        self.host = host
        self.socket = None
        self.rsa_pub = None
        self.send(dict(query='RSA_GET'))

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def send(self, data):
        try:
            self.connect()
            send_data = Request(data, rsa_pub=self.rsa_pub).encode()
            self.socket.sendall(send_data)
            data = self.socket.recv(self.MAX_BUFFER_SIZE)
            parsed_data = Response(data=data).parse()
            if 'rsa' in parsed_data:
                self.rsa_pub = parsed_data['rsa']
            if not parsed_data or parsed_data['status'] != Response.SUCCESS_STATUS:
                print messages.FAILED_SEND
        except socket.error:
            print messages.CONNECT_EXCEPTION
        finally:
            self.socket.close()

    def __str__(self):
        return '{0}:{1}'.format(self.host if self.host else '127.0.0.1', self.port)


class ClientManager(object):
    clients = dict()

    def add(self, name, port, host):
        self.clients[name] = Client(port, host)

    def get(self, name):
        return self.clients.get(name, None)

    def show(self):
        printer_clients = ['{0} {1}'.format(name, self.clients[name]) for name in self.clients]
        print '\n'.join(printer_clients)
