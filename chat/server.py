from threading import Thread
import socket
import sys

from utils import messages, RSA
from base import Response, Request


class Server(Thread):
    MAX_BUFFER_SIZE = 8096

    def __init__(self, port, host):
        super(Server, self).__init__()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_alive = True
        self.own_address = (host, port)
        self.rsa = RSA.get_rsa_key()[0]
        try:
            self.socket.bind(self.own_address)
        except OSError:
            print messages.BIND_PORT_EXCEPTION
            sys.exit(0)
        self.socket.listen(1)

    def run(self):
        while self.server_alive:
            try:
                conn, address = self.socket.accept()
                data = conn.recv(self.MAX_BUFFER_SIZE)
                status = Response.SUCCESS_STATUS
                response_context = dict()
                if not data:
                    status = Response.FAILED_STATUS
                else:
                    data = Request(data, rsa=self.rsa).parse()
                    if data.get('query', '') == 'RSA_GET':
                        response_context['rsa'] = RSA.get_rsa_key()[1]
                    else:
                        self.data_printer(address, data)
                conn.sendall(Response(status=status, data=response_context).encode())
                conn.close()
            except Exception:
                self.stop()

    @staticmethod
    def data_printer(address, data):
        print messages.MESSAGE.format(data.get('message', ''), *address)

    def stop(self):
        self.__del__()

    def __del__(self):
        self.server_alive = False
        try:
            socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(self.own_address)
            self.socket.close()
        except socket.error:
            pass
